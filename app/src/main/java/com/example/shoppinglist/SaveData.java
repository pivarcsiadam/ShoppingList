package com.example.shoppinglist;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;

import java.util.ArrayList;

public class SaveData {

    public static void saveData(SharedPreferences sharedPreferences, ArrayList<Item> items){
        @SuppressLint("CommitPrefEdits") SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        int i = 0;
        for (Item item: items){
            editor.putString("name" + i, item.getName());
            editor.putString("unit" + i, item.getUnit());
            editor.putFloat("inList" + i, item.getInListQuantity());
            editor.putFloat("inCart" + i, item.getInCartQuantity());
            editor.putFloat("price" + i, item.getPrice());
            i++;
        }
        editor.apply();
    }

    public static ArrayList<Item> getData(SharedPreferences sharedPreferences){
        ArrayList<Item> items = new ArrayList<>();
        boolean loop = true;
        int i = 0;
        while(loop){
            if (sharedPreferences.contains("name" + i)){
                String name = sharedPreferences.getString("name" + i, null);
                String unit = sharedPreferences.getString("unit" + i, null);
                float inList = sharedPreferences.getFloat("inList" + i, 0);
                float inCart = sharedPreferences.getFloat("inCart" + i, 0);
                float price = sharedPreferences.getFloat("price" + i, 0);

                Item item = new Item(name, inList, unit);
                item.setInCartQuantity(inCart);
                item.setPrice(price);
                items.add(item);
                i++;
            } else {
                loop = false;
            }
        }
        return items;
    }
}
