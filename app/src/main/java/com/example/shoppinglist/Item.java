package com.example.shoppinglist;

import android.os.Parcel;
import android.os.Parcelable;

public class Item implements Parcelable {
    private String name;
    private String unit;
    private float inListQuantity;
    private float inCartQuantity = 0;
    private float price = 0;

    public Item(String name, float inListQuantity, String unit){
        this.name = name;
        this.inListQuantity = inListQuantity;
        this.unit = unit;
    }

    protected Item(Parcel in) {
        name = in.readString();
        inListQuantity = in.readFloat();
        inCartQuantity = in.readFloat();
        unit = in.readString();
        price = in.readFloat();
    }

    public static final Creator<Item> CREATOR = new Creator<Item>() {
        @Override
        public Item createFromParcel(Parcel in) {
            return new Item(in);
        }

        @Override
        public Item[] newArray(int size) {
            return new Item[size];
        }
    };


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getInListQuantity() {
        return inListQuantity;
    }

    public void setInListQuantity(float inListQuantity) {
        this.inListQuantity = inListQuantity;
    }

    public float getInCartQuantity() {
        return inCartQuantity;
    }

    public void setInCartQuantity(float inCartQuantity) {
        this.inCartQuantity = inCartQuantity;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeFloat(inListQuantity);
        parcel.writeFloat(inCartQuantity);
        parcel.writeString(unit);
        parcel.writeFloat(price);
    }
}
