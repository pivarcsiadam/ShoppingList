package com.example.shoppinglist;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    public static final int TEXT_REQUEST = 1;
    private ArrayList<Item> items = new ArrayList<>();
    private Item selectedItem;
    private String selectedItemUnit;
    private LinearLayout table;
    private LinearLayout mSelectedItem;
    private LinearLayout mItemMenu;
    private ConstraintLayout mDeleteAlertBox;
    private ConstraintLayout mAddCartMenu;
    private ConstraintLayout mEditItemMenu;
    private TextView mFullPrice;
    private ConstraintLayout mMask;
    private Button mFlag;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLocal();
        setContentView(R.layout.activity_main);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(getResources().getString(R.string.app_name));
        }
        mFlag = findViewById(R.id.language_button);
        changeFlag();
        table = findViewById(R.id.table_content);
        mItemMenu = findViewById(R.id.item_menu_widget);
        mDeleteAlertBox = findViewById(R.id.delete_alert_box);
        mAddCartMenu = findViewById(R.id.add_cart_modal);
        mFullPrice = findViewById(R.id.full_price);
        mEditItemMenu = findViewById(R.id.edit_item_modal);
        mMask = findViewById(R.id.mask);
        getData();
        buildList();
        mFullPrice.setText(String.valueOf(countFullPrice()));
    }

    public void addItem(View view) {
        Intent intent = new Intent(this, ItemsActivity.class);
        saveData();
        intent.putExtra("items", items);
        startActivityForResult(intent, TEXT_REQUEST);
    }

    public void clearPayedItem(View view){
        clearItemList();
        table.removeAllViews();
        buildList();
        mFullPrice.setText("0");
    }

    public void showLanguageDialog(View view){
        final String[] listLanguages = {"English(US)", "Deutsche", "Magyar"};
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(MainActivity.this);
        mBuilder.setTitle(R.string.language_title);
        mBuilder.setSingleChoiceItems(listLanguages, -1, (dialogInterface, i) -> {
            if (i == 0){
                mFlag.setBackgroundResource(R.drawable.united_states_flag);
                setLocal("en");
                recreate();
            } else if (i == 1){
                mFlag.setBackgroundResource(R.drawable.germany_flag);
                setLocal("de");
                recreate();
            } else if (i == 2){
                mFlag.setBackgroundResource(R.drawable.hungary_flag);
                setLocal("hu");
                recreate();
            }
            dialogInterface.dismiss();
        });
        AlertDialog mDialog = mBuilder.create();
        mDialog.show();
    }

    private void setLocal(String lang){
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        @SuppressLint("CommitPrefEdits") SharedPreferences.Editor editor = getSharedPreferences("Settings", MODE_PRIVATE).edit();
        editor.putString("My_lang", lang);
        editor.apply();
    }

    public void loadLocal(){
        SharedPreferences pref = getSharedPreferences("Settings", Activity.MODE_PRIVATE);
        String language = pref.getString("My_lang", "");
        setLocal(language);
    }

    @SuppressLint("SetTextI18n")
    public void openAddCartMenu(View view){
        mItemMenu.setVisibility(View.INVISIBLE);
        ((TextView)mAddCartMenu.findViewById(R.id.add_cart_item_name)).setText(selectedItem.getName());
        ((TextView)mAddCartMenu.findViewById(R.id.add_cart_in_list_item)).setText(selectedItem.getInListQuantity() + "/" + selectedItem.getUnit());
        ((TextView)mAddCartMenu.findViewById(R.id.add_cart_unit)).setText(selectedItem.getUnit());
        if (selectedItem.getInCartQuantity() != 0 && selectedItem.getPrice() != 0){
            ((EditText)mAddCartMenu.findViewById(R.id.add_cart_add_item_quantity)).setText(String.valueOf(selectedItem.getInCartQuantity()));
            ((EditText)mAddCartMenu.findViewById(R.id.add_cart_price)).setText(String.valueOf(selectedItem.getPrice()));
        }
        mAddCartMenu.setVisibility(View.VISIBLE);
    }

    public void backToItemMenuFromAddCartMenu(View view){
        mAddCartMenu.setVisibility(View.INVISIBLE);
        mItemMenu.setVisibility(View.VISIBLE);
    }

    @SuppressLint("SetTextI18n")
    public void addIntoCart(View view){
        float inCartQuantity;
        float price;
        try{
            inCartQuantity = Float.parseFloat(((TextView)findViewById(R.id.add_cart_add_item_quantity)).getText().toString());
            price = Float.parseFloat(((TextView)findViewById(R.id.add_cart_price)).getText().toString());
            selectedItem.setInCartQuantity(inCartQuantity);
            selectedItem.setPrice(price);
            ((TextView)mSelectedItem.findViewById(R.id.in_cart_quantity)).setText(inCartQuantity + "\n/" + selectedItem.getUnit());
            ((TextView)mSelectedItem.findViewById(R.id.unit_price)).setText(String.valueOf(price));
            mSelectedItem.setBackgroundResource(getDrawableId(selectedItem));
            mAddCartMenu.setVisibility(View.INVISIBLE);
            mItemMenu.setVisibility(View.VISIBLE);
            mFullPrice.setText(String.valueOf(countFullPrice()));
            ((EditText)mAddCartMenu.findViewById(R.id.add_cart_add_item_quantity)).setText("");
            ((EditText)mAddCartMenu.findViewById(R.id.add_cart_price)).setText("");
            saveData();
        } catch (Exception e){
            Toast.makeText(this, R.string.missing_data_alert, Toast.LENGTH_SHORT).show();
        }

    }

    public void openEditMenu(View view){
        mItemMenu.setVisibility(View.INVISIBLE);
        ((TextView)mEditItemMenu.findViewById(R.id.edit_item_name)).setText(selectedItem.getName());
        ((TextView)mEditItemMenu.findViewById(R.id.edit_item_quantity)).setText(String.valueOf(selectedItem.getInListQuantity()));
        mEditItemMenu.findViewById(getUnitId(selectedItem.getUnit())).setBackgroundResource(R.drawable.unit_button_selected);
        selectedItemUnit = selectedItem.getUnit();
        mEditItemMenu.setVisibility(View.VISIBLE);
    }

    public void editItemSelectUnit(View view){
        if(selectedItemUnit != null){
            findViewById(getUnitId(selectedItemUnit)).setBackgroundResource(R.drawable.button_with_border);
        }
        selectedItemUnit = ((Button) view).getText().toString();
        view.setBackgroundResource(R.drawable.unit_button_selected);
    }

    @SuppressLint("SetTextI18n")
    public void saveEditItem(View view){
        String itemName;
        float inListQuantity;
        try{
            itemName = ((TextView)findViewById(R.id.edit_item_name)).getText().toString();
            inListQuantity = Float.parseFloat(((TextView)findViewById(R.id.edit_item_quantity)).getText().toString());
            if (checkRepeatItem(itemName) && !itemName.equals("")){
                selectedItem.setName(itemName);
                selectedItem.setInListQuantity(inListQuantity);
                selectedItem.setUnit(selectedItemUnit);
                ((TextView)mSelectedItem.findViewById(R.id.item_name)).setText(itemName);
                ((TextView)mSelectedItem.findViewById(R.id.in_list_quantity)).setText(inListQuantity + "\n/" + selectedItem.getUnit());
                ((TextView)mSelectedItem.findViewById(R.id.in_cart_quantity)).setText(selectedItem.getInCartQuantity() + "\n/" + selectedItem.getUnit());
                mSelectedItem.setBackgroundResource(getDrawableId(selectedItem));
                mEditItemMenu.setVisibility(View.INVISIBLE);
                mItemMenu.setVisibility(View.VISIBLE);
                saveData();
            } else if(itemName.equals("")){
                Toast.makeText(this, R.string.missing_data_alert, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, R.string.item_repeat_message, Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e){
            Toast.makeText(this, R.string.missing_data_alert, Toast.LENGTH_SHORT).show();
        }
    }

    public void backToItemMenuFromEditItemMenu(View view){
        mEditItemMenu.setVisibility(View.INVISIBLE);
        mItemMenu.setVisibility(View.VISIBLE);
    }

    public void closeItemMenu(View view){
        mItemMenu.setVisibility(View.INVISIBLE);
        clearMask();
        selectedItem = null;
        mSelectedItem = null;
    }

    public void deleteItem(View view){
       mDeleteAlertBox.setVisibility(View.VISIBLE);
    }

    public void acceptDeleteItem(View view){
        items.remove(selectedItem);
        ((ViewManager)mSelectedItem.getParent()).removeView(mSelectedItem);
        mDeleteAlertBox.setVisibility(View.INVISIBLE);
        mItemMenu.setVisibility(View.INVISIBLE);
        mFullPrice.setText(String.valueOf(countFullPrice()));
        clearMask();
        selectedItem = null;
        mSelectedItem = null;
        saveData();
    }

    public void rejectDeleteItem(View view){
        mDeleteAlertBox.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onSaveInstanceState(Bundle bundle){
        super.onSaveInstanceState(bundle);
        bundle.putParcelableArrayList("items", items);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == TEXT_REQUEST){
            if(resultCode == RESULT_OK){
                if (data.getBooleanExtra(ItemsActivity.EXTRA_NO_ITEM, false)){
                    items.add(data.getParcelableExtra(ItemsActivity.EXTRA_ITEM));
                    saveData();
                    addNewItem(items.get(items.size() - 1));
                }
            }
        }
    }

    private void clearItemList(){
        ArrayList<Item> deletedItems = new ArrayList<>();
        for (Item item: items){
            if (item.getInListQuantity() <= item.getInCartQuantity()){
                deletedItems.add(item);
            } else if(item.getInListQuantity() > item.getInCartQuantity() && item.getInCartQuantity() != 0){
                item.setInListQuantity(item.getInListQuantity() - item.getInCartQuantity());
                item.setInCartQuantity(0);
                item.setPrice(0);
            }
        }
        for (Item item: deletedItems){
            items.remove(item);
        }
        saveData();
    }

    private void buildList(){
        for(Item item: items){
            addNewItem(item);
        }
    }

    private float countFullPrice(){
        float price = 0;
        if(items.size() != 0){
            for(Item item: items){
                price += item.getInCartQuantity() * item.getPrice();
            }

        }
        return price;
    }

    @SuppressLint("SetTextI18n")
    private void addNewItem(Item item){
        @SuppressLint("InflateParams") View row = getLayoutInflater().inflate(R.layout.new_item_form, null, false);

        TextView itemName = row.findViewById(R.id.item_name);
        TextView inListQuantity = row.findViewById(R.id.in_list_quantity);
        TextView inCartQuantity = row.findViewById(R.id.in_cart_quantity);
        TextView price = row.findViewById(R.id.unit_price);

        itemName.setText(item.getName());
        itemName.setPadding(8, 4, 4, 4);
        inListQuantity.setText(item.getInListQuantity() + "\n/" + item.getUnit());
        inCartQuantity.setText(item.getInCartQuantity() + "\n/" + item.getUnit());
        price.setText(String.valueOf(item.getPrice()));

        row.setBackgroundResource(getDrawableId(item));
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 140);
        layoutParams.setMargins(4, 4, 4, 4);
        row.setLayoutParams(layoutParams);
        row.setOnClickListener(view -> {
            mItemMenu.setVisibility(View.VISIBLE);
            displayMask();
            String itemName1 = ((TextView)view.findViewById(R.id.item_name)).getText().toString();
            selectedItem = searchItemByName(itemName1);
            mSelectedItem = (LinearLayout)view;
        });
        table.addView(row);
    }

    private Item searchItemByName(String itemName){
        for (Item item: items){
            if(item.getName().equals(itemName)){
                return item;
            }
        }
        return null;
    }

    private int getDrawableId(Item item) {
        int drawableId;
        if (item.getInListQuantity() <= item.getInCartQuantity()){
            drawableId = R.drawable.item_no_missing;
        } else if(item.getInCartQuantity() == 0){
            drawableId = R.drawable.item_missing;
        } else {
            drawableId = R.drawable.item_part_missing;
        }
        return drawableId;
    }

    private int getUnitId(String unit) {
        int id;
        if (unit.equals(getString(R.string.add_item_weight))){
            id = R.id.edit_weight_unit;
        } else if (unit.equals(getString(R.string.add_item_quantity))){
            id = R.id.edit_quantity_unit;
        } else {
            id = R.id.edit_liter_unit;
        }
        return id;
    }

    private boolean checkRepeatItem(String itemName){
        for (Item item: items){
            if(itemName.equals(item.getName()) && item != selectedItem){
                return false;
            }
        }
        return true;
    }

    private void displayMask(){
        mMask.setVisibility(View.VISIBLE);
        mMask.setFocusable(true);
    }

    private void clearMask(){
        mMask.setVisibility(View.INVISIBLE);
        mMask.setFocusable(false);
    }

    private void saveData(){
        SharedPreferences sharedPreferences = getSharedPreferences("Data", 0);
        SaveData.saveData(sharedPreferences, items);
    }

    private void getData(){
        SharedPreferences sharedPreferences = getSharedPreferences("Data", 0);
        items = SaveData.getData(sharedPreferences);
    }

    private void changeFlag(){
        SharedPreferences pref = getSharedPreferences("Settings", Activity.MODE_PRIVATE);
        String language = pref.getString("My_lang", "");
        switch (language){
            case "hu":
                mFlag.setBackgroundResource(R.drawable.hungary_flag);
                break;
            case "de":
                mFlag.setBackgroundResource(R.drawable.germany_flag);
                break;
            case "en":
                mFlag.setBackgroundResource(R.drawable.united_states_flag);
                break;
        }
    }

}