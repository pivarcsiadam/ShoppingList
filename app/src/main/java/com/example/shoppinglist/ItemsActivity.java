package com.example.shoppinglist;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;


public class ItemsActivity extends AppCompatActivity {
    public static final String EXTRA_ITEM = "com.example.shoppinglist.ITEM";
    public static final String EXTRA_NO_ITEM = "com.example.shoppinglist.NO_ITEM";
    private ArrayList<Item> items;
    private String unit;
    private EditText mItemName;
    private EditText mItemQuantity;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLocal();
        setContentView(R.layout.activity_items);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(getResources().getString(R.string.app_name));
        }
        mItemName = findViewById(R.id.item_name_field);
        mItemQuantity = findViewById(R.id.item_quantity_field);
        Intent intent = getIntent();
        items = intent.getParcelableArrayListExtra("items");
        if(savedInstanceState != null){
            unit = savedInstanceState.getString("unit");
            if(unit != null){
                findViewById(getUnitId()).setBackgroundResource(R.drawable.unit_button_selected);
            }
        }
    }

    public void selectUnit(View view){
        if(unit != null){
            findViewById(getUnitId()).setBackgroundResource(R.drawable.button_with_border);
        }
        unit = ((Button) view).getText().toString();
        view.setBackgroundResource(R.drawable.unit_button_selected);
    }

    private void setLocal(String lang){
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        @SuppressLint("CommitPrefEdits") SharedPreferences.Editor editor = getSharedPreferences("Settings", MODE_PRIVATE).edit();
        editor.putString("My_lang", lang);
        editor.apply();
    }

    public void loadLocal(){
        SharedPreferences pref = getSharedPreferences("Settings", Activity.MODE_PRIVATE);
        String language = pref.getString("My_lang", "");
        setLocal(language);
    }

    private int getUnitId() {
        int id;
        if (unit.equals(getString(R.string.add_item_weight))){
            id = R.id.item_unit_weight;
        } else if (unit.equals(getString(R.string.add_item_quantity))){
            id = R.id.item_unit_quantity;
        } else {
            id = R.id.item_unit_liter;
        }
        return id;
    }

    public void addItem(View view) {
        String itemName = mItemName.getText().toString();
        float itemQuantity = 0;
        try{
            itemQuantity = Float.parseFloat(mItemQuantity.getText().toString());
        } catch (Exception ignore){}
        if(checkItemRepeat(itemName)){
            Toast.makeText(this, R.string.item_repeat_message, Toast.LENGTH_SHORT).show();
        } else if(unit != null && !itemName.equals("") && itemQuantity != 0){
            Intent intent = new Intent();
            Item item = new Item(itemName, itemQuantity, unit);
            intent.putExtra(EXTRA_ITEM, item);
            intent.putExtra(EXTRA_NO_ITEM, true);
            setResult(RESULT_OK, intent);
            finish();
        } else {
            Toast.makeText(this, R.string.add_item_alert_message, Toast.LENGTH_SHORT).show();
        }
    }

    public void backToMainActivity(View view){
        Intent intent = new Intent();
        intent.putExtra(EXTRA_NO_ITEM, false);
        setResult(RESULT_OK, intent);
        finish();
    }

    private boolean checkItemRepeat(String itemName){
        for(Item item: items){
            if(item.getName().toUpperCase().equals(itemName.toUpperCase())){
                return true;
            }
        }
        return false;
    }

    @Override
    public void onSaveInstanceState(Bundle bundle){
        super.onSaveInstanceState(bundle);
        bundle.putString("unit", unit);
    }
}