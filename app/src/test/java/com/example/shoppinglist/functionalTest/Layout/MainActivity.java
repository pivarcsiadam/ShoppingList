package com.example.shoppinglist.functionalTest.Layout;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class MainActivity extends Layout{
    @AndroidFindBy(id = "add_button")
    MobileElement addButton;
    @AndroidFindBy(id = "table_content")
    MobileElement tableList;
    @AndroidFindBy(id = "language_button")
    MobileElement changeLanguageButton;
    @AndroidFindBy(id = "paying")
    MobileElement payingButton;
    @AndroidFindBy(id = "full_price")
    MobileElement fullPrice;
    @AndroidFindBy(id = "com.example.shoppinglist:id/alertTitle")
    MobileElement alertTitle;
    @AndroidFindBy(id = "com.example.shoppinglist:id/select_dialog_listview")
    MobileElement languageList;

    public MainActivity(AndroidDriver<AndroidElement> driver) {
        super(driver);
    }

    public void clickAddButton(){
        addButton.click();
    }

    public void waitForActivity(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("add_button")));
    }

    public boolean checkItemDisplayByName(String itemName){
        String xpath = String.format("//android.widget.TextView[@text='%s']", itemName.toUpperCase());
        return tableList.findElements(By.xpath(xpath)).size() == 1;
    }

    public boolean checkItemInListQuantity(String itemName, float expectedQuantity, String expectedUnit){
        MobileElement item = getElementByName(itemName);
        String quantityWithUnit = item.findElement(By.xpath("//android.view.ViewGroup[@resource-id='com.example.shoppinglist:id/item_in_list_field']/android.widget.TextView")).getText();
        float quantity = Float.parseFloat(quantityWithUnit.split("/")[0]);
        String unit = quantityWithUnit.split("/")[1].replace("\n", "");
        return quantity == expectedQuantity && unit.toUpperCase().equals(expectedUnit.toUpperCase());
    }

    public boolean checkItemInCartQuantity(String itemName, float checkQuantity, String checkUnit){
        MobileElement item = getElementByName(itemName);
        String quantityWithUnit = item.findElement(By.xpath("//android.view.ViewGroup[@resource-id='com.example.shoppinglist:id/item_in_cart_field']/android.widget.TextView")).getText();
        float quantity = Float.parseFloat(quantityWithUnit.split("/")[0]);
        String unit = quantityWithUnit.split("/")[1].replace("\n", "");
        return quantity == checkQuantity && unit.toUpperCase().equals(checkUnit.toUpperCase());
    }

    public boolean checkPrice(String itemName, float checkPrice){
        MobileElement item = getElementByName(itemName);
        float price = Float.parseFloat(item.findElement(By.xpath("//android.view.ViewGroup[@resource-id='com.example.shoppinglist:id/item_price_field']/android.widget.TextView")).getText());
        return price == checkPrice;
    }

    public void clickElement(String name){
        getElementByName(name).click();
    }

    private MobileElement getElementByName(String itemName){
        MobileElement item = null;
        String xpath = String.format("//android.widget.TextView[@text='%s']/parent::android.view.ViewGroup/parent::android.widget.LinearLayout", itemName.toUpperCase());
        try{
            item = tableList.findElement(By.xpath(xpath));
        } catch (Exception ignore){}
        return item;
    }

    public boolean checkElementDisappearByName(String name){
        return getElementByName(name) == null;
    }

    public void clickLanguageButton(){
        changeLanguageButton.click();
    }

    public void waitLanguageChangeBoxByLanguageMessage(){
        wait.until(ExpectedConditions.visibilityOf(alertTitle));
    }

    public void clickLanguage(String language){
        switch (language){
            case "English(US)":
                languageList.findElement(By.xpath("//android.widget.CheckedTextView[1]")).click();
                break;
            case "Deutsche":
                languageList.findElement(By.xpath("//android.widget.CheckedTextView[2]")).click();
                break;
            case "Magyar":
                languageList.findElement(By.xpath("//android.widget.CheckedTextView[3]")).click();
                break;
        }
    }

    public String getAddButtonText(){
        return addButton.getText();
    }

    public void waitForToast(){
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/hierarchy/android.widget.Toast")));
    }

    public String getToastMessage(){
        return driver.findElement(By.xpath("/hierarchy/android.widget.Toast")).getText();
    }

    public float getFullPrice(){
        return Float.parseFloat(fullPrice.getText());
    }

    public void clickPayingButton(){
        payingButton.click();
    }
}
