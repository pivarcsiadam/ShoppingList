package com.example.shoppinglist.functionalTest.Layout;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class ItemMenuModal extends Layout{
    @AndroidFindBy(id = "item_menu_into_cart")
    MobileElement intoCartButton;
    @AndroidFindBy(id = "item_menu_edit")
    MobileElement editButton;
    @AndroidFindBy(id = "item_menu_delete")
    MobileElement deleteButton;
    @AndroidFindBy(id = "item_menu_back")
    MobileElement backButton;

    public ItemMenuModal(AndroidDriver<AndroidElement> driver) {
        super(driver);
    }

    public void waitForModal(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("item_menu_back")));
    }

    public void clickIntoCartButton(){
        intoCartButton.click();
    }

    public void clickEditButton(){
        editButton.click();
    }

    public void clickDeleteButton(){
        deleteButton.click();
    }

    public void clickBackButton(){
        backButton.click();
    }


}
