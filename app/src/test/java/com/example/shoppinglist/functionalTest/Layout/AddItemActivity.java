package com.example.shoppinglist.functionalTest.Layout;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class AddItemActivity extends Layout{

    @AndroidFindBy(id = "item_name_field")
    MobileElement itemNameField;
    @AndroidFindBy(id = "item_quantity_field")
    MobileElement itemQuantityField;
    @AndroidFindBy(id = "item_unit_quantity")
    MobileElement itemUnitQuantityButton;
    @AndroidFindBy(id = "item_unit_weight")
    MobileElement itemUnitWeightButton;
    @AndroidFindBy(id = "item_unit_liter")
    MobileElement itemUnitLiterButton;
    @AndroidFindBy(id = "item_add_button")
    MobileElement addItemButton;
    @AndroidFindBy(id = "back_to_main_activity")
    MobileElement backButton;


    public AddItemActivity(AndroidDriver<AndroidElement> driver) {
        super(driver);
    }

    public void waitForActivity(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("item_name_field")));
    }

    public void typeItemName(String itemName){
        itemNameField.sendKeys(itemName);
    }

    public void typeItemQuantity(float quantity){
        itemQuantityField.sendKeys(String.valueOf(quantity));
    }

    public void selectUnitWeight(){
        itemUnitWeightButton.click();
    }

    public void selectUnitQuantity(){
        itemUnitQuantityButton.click();
    }

    public void selectUnitLiter(){
        itemUnitLiterButton.click();
    }

    public void clickAddButton(){
        addItemButton.click();
    }

    public void waitForToast(){
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/hierarchy/android.widget.Toast")));
    }

    public String getToastMessage(){
        return driver.findElement(By.xpath("/hierarchy/android.widget.Toast")).getText();
    }

    public void backToMainActivity(){
        backButton.click();
    }
}
