package com.example.shoppinglist.functionalTest.Tests;

import com.example.shoppinglist.functionalTest.Layout.AddItemActivity;
import com.example.shoppinglist.functionalTest.Layout.DeleteAlertBox;
import com.example.shoppinglist.functionalTest.Layout.ItemMenuModal;
import com.example.shoppinglist.functionalTest.Layout.MainActivity;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class AddNewItemTest extends TestBody{
    private MainActivity mainActivity;
    private AddItemActivity addItemActivity;
    private ItemMenuModal itemMenuModal;
    private DeleteAlertBox deleteAlertBox;

    @BeforeEach
    public void setupLayout(){
        mainActivity = new MainActivity(driver);
        addItemActivity = new AddItemActivity(driver);
        itemMenuModal = new ItemMenuModal(driver);
        deleteAlertBox = new DeleteAlertBox(driver);
    }

    @Test
    public void addItemAndDeleteTestHappyWay(){
        mainActivity.waitForActivity();
        mainActivity.clickAddButton();
        addItemActivity.waitForActivity();
        addItemActivity.typeItemName("Peach");
        addItemActivity.typeItemQuantity(4);
        addItemActivity.selectUnitQuantity();
        addItemActivity.clickAddButton();
        mainActivity.waitForActivity();
        Assertions.assertTrue(mainActivity.checkItemDisplayByName("Peach"));
        Assertions.assertTrue(mainActivity.checkItemInListQuantity("Peach", 4, "DB"));
        Assertions.assertTrue(mainActivity.checkItemInCartQuantity("Peach", 0, "DB"));
        Assertions.assertTrue(mainActivity.checkPrice("Peach", 0));
        mainActivity.clickElement("Peach");
        itemMenuModal.clickDeleteButton();
        deleteAlertBox.acceptDelete();
        Assertions.assertTrue(mainActivity.checkElementDisappearByName("Peach"));
    }

    @Test
    public void addItemFailWithEmptyName(){
        mainActivity.waitForActivity();
        mainActivity.clickAddButton();
        addItemActivity.waitForActivity();
        addItemActivity.typeItemQuantity(4);
        addItemActivity.selectUnitQuantity();
        addItemActivity.clickAddButton();
        addItemActivity.waitForToast();
        Assertions.assertEquals(addItemActivity.getToastMessage(), "Kérlek adj meg minden adatot!");
    }

    @Test
    public void addItemFailWithEmptyQuantity(){
        mainActivity.waitForActivity();
        mainActivity.clickAddButton();
        addItemActivity.waitForActivity();
        addItemActivity.typeItemName("Peach");
        addItemActivity.selectUnitQuantity();
        addItemActivity.clickAddButton();
        addItemActivity.waitForToast();
        Assertions.assertEquals(addItemActivity.getToastMessage(), "Kérlek adj meg minden adatot!");
    }

    @Test
    public void addItemFailWithoutUnit(){
        mainActivity.waitForActivity();
        mainActivity.clickAddButton();
        addItemActivity.waitForActivity();
        addItemActivity.typeItemName("Peach");
        addItemActivity.typeItemQuantity(4);
        addItemActivity.clickAddButton();
        addItemActivity.waitForToast();
        Assertions.assertEquals(addItemActivity.getToastMessage(), "Kérlek adj meg minden adatot!");
    }

    @Test
    public void addItemFailRepeatedItem(){
        mainActivity.waitForActivity();
        mainActivity.clickAddButton();
        addItemActivity.waitForActivity();
        addItemActivity.typeItemName("Peach");
        addItemActivity.typeItemQuantity(4);
        addItemActivity.selectUnitQuantity();
        addItemActivity.clickAddButton();
        mainActivity.waitForActivity();
        Assertions.assertTrue(mainActivity.checkItemDisplayByName("Peach"));
        mainActivity.clickAddButton();
        addItemActivity.waitForActivity();
        addItemActivity.typeItemName("Peach");
        addItemActivity.typeItemQuantity(2);
        addItemActivity.selectUnitWeight();
        addItemActivity.clickAddButton();
        addItemActivity.waitForToast();
        Assertions.assertEquals(addItemActivity.getToastMessage(), "Ezt a terméket már hozzáadtad a listához!");
        addItemActivity.backToMainActivity();
        mainActivity.waitForActivity();
        mainActivity.clickElement("Peach");
        itemMenuModal.clickDeleteButton();
        deleteAlertBox.acceptDelete();
    }

    @Test
    public void rejectDeleteItemTest(){
        mainActivity.waitForActivity();
        mainActivity.clickAddButton();
        addItemActivity.waitForActivity();
        addItemActivity.typeItemName("Peach");
        addItemActivity.typeItemQuantity(4);
        addItemActivity.selectUnitQuantity();
        addItemActivity.clickAddButton();
        mainActivity.waitForActivity();
        Assertions.assertTrue(mainActivity.checkItemDisplayByName("Peach"));
        mainActivity.clickElement("Peach");
        itemMenuModal.clickDeleteButton();
        deleteAlertBox.rejectDelete();
        itemMenuModal.clickBackButton();
        Assertions.assertTrue(mainActivity.checkItemDisplayByName("Peach"));
        mainActivity.clickElement("Peach");
        itemMenuModal.clickDeleteButton();
        deleteAlertBox.acceptDelete();
    }


}
