package com.example.shoppinglist.functionalTest.Tests;

import com.example.shoppinglist.functionalTest.Layout.AddCartModal;
import com.example.shoppinglist.functionalTest.Layout.AddItemActivity;
import com.example.shoppinglist.functionalTest.Layout.DeleteAlertBox;
import com.example.shoppinglist.functionalTest.Layout.ItemMenuModal;
import com.example.shoppinglist.functionalTest.Layout.MainActivity;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class AdIntoCartTest extends TestBody {
    private MainActivity mainActivity;
    private ItemMenuModal itemMenuModal;
    private DeleteAlertBox deleteAlertBox;
    private AddCartModal addCartModal;

    @BeforeEach
    public void setupPrecondition(){
        mainActivity = new MainActivity(driver);
        AddItemActivity addItemActivity = new AddItemActivity(driver);
        itemMenuModal = new ItemMenuModal(driver);
        deleteAlertBox = new DeleteAlertBox(driver);
        addCartModal = new AddCartModal(driver);

        mainActivity.waitForActivity();
        mainActivity.clickAddButton();
        addItemActivity.waitForActivity();
        addItemActivity.typeItemName("Peach");
        addItemActivity.typeItemQuantity(4);
        addItemActivity.selectUnitLiter();
        addItemActivity.clickAddButton();
        mainActivity.waitForActivity();
        Assertions.assertTrue(mainActivity.checkItemDisplayByName("Peach"));
    }

    @Test
    public void adToCartHappyWay(){
        mainActivity.clickElement("Peach");
        itemMenuModal.clickIntoCartButton();
        Assertions.assertEquals(addCartModal.getItemName(), "PEACH");
        Assertions.assertEquals(addCartModal.getItemQuantityAndUnit(), 4.0 + "/L");
        addCartModal.typeInCartField(4);
        addCartModal.typePrice(200);
        addCartModal.clickAddButton();
        itemMenuModal.clickBackButton();
        Assertions.assertTrue(mainActivity.checkItemInCartQuantity("Peach", 4, "L"));
        Assertions.assertTrue(mainActivity.checkPrice("Peach", 200));
    }

    @Test
    public void adToCartFailWithEmptyQuantity(){
        mainActivity.clickElement("Peach");
        itemMenuModal.clickIntoCartButton();
        addCartModal.typePrice(200);
        addCartModal.clickAddButton();
        mainActivity.waitForToast();
        Assertions.assertEquals(mainActivity.getToastMessage(), "Ne hagyj üresen mezőt!");
        addCartModal.clickBackButton();
        itemMenuModal.clickBackButton();
    }

    @Test
    public void adToCartFailWithEmptyPrice(){
        mainActivity.clickElement("Peach");
        itemMenuModal.clickIntoCartButton();
        addCartModal.typeInCartField(4);
        addCartModal.clickAddButton();
        mainActivity.waitForToast();
        Assertions.assertEquals(mainActivity.getToastMessage(), "Ne hagyj üresen mezőt!");
        addCartModal.clickBackButton();
        itemMenuModal.clickBackButton();
    }

    @Test
    public void adToCartCancel(){
        mainActivity.clickElement("Peach");
        itemMenuModal.clickIntoCartButton();
        Assertions.assertEquals(addCartModal.getItemName(), "PEACH");
        Assertions.assertEquals(addCartModal.getItemQuantityAndUnit(), 4.0 + "/L");
        addCartModal.typeInCartField(4);
        addCartModal.typePrice(200);
        addCartModal.clickBackButton();
        itemMenuModal.clickBackButton();
        Assertions.assertTrue(mainActivity.checkItemInCartQuantity("Peach", 0, "L"));
        Assertions.assertTrue(mainActivity.checkPrice("Peach", 0));
    }

    @AfterEach
    public void clearTable(){
        mainActivity.clickElement("Peach");
        itemMenuModal.waitForModal();
        itemMenuModal.clickDeleteButton();
        deleteAlertBox.acceptDelete();
        Assertions.assertTrue(mainActivity.checkElementDisappearByName("Peach"));
    }
}
