package com.example.shoppinglist.functionalTest.Layout;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class EditItemModal extends Layout {

    @AndroidFindBy(id = "edit_item_name")
    MobileElement itemNameField;
    @AndroidFindBy(id = "edit_item_quantity")
    MobileElement itemQuantityField;
    @AndroidFindBy(id = "edit_weight_unit")
    MobileElement weightUnitButton;
    @AndroidFindBy(id = "edit_save_button")
    MobileElement saveButton;
    @AndroidFindBy(id = "edit_back_button")
    MobileElement backButton;

    public EditItemModal(AndroidDriver<AndroidElement> driver) {
        super(driver);
    }

    public void clickSaveButton(){
        saveButton.click();
    }

    public void clickBackButton(){
        backButton.click();
    }

    public String getItemName(){
        return itemNameField.getText();
    }

    public float getItemQuantity(){
        return Float.parseFloat(itemQuantityField.getText());
    }

    public void editItemName(String newName){
        itemNameField.clear();
        itemNameField.sendKeys(newName);
    }

    public void editInListQuantity(float newQuantity){
        itemQuantityField.clear();
        itemQuantityField.sendKeys(String.valueOf(newQuantity));
    }

    public void clearQuantityField(){
        itemQuantityField.clear();
    }

    public void selectWeighUnit(){
        weightUnitButton.click();
    }
}
