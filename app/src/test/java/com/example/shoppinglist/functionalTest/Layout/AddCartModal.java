package com.example.shoppinglist.functionalTest.Layout;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class AddCartModal extends Layout {

    @AndroidFindBy(id = "add_cart_item_name")
    MobileElement itemName;
    @AndroidFindBy(id = "add_cart_in_list_item")
    MobileElement inListQuantity;
    @AndroidFindBy(id = "add_cart_add_item_quantity")
    MobileElement inCartQuantityField;
    @AndroidFindBy(id = "add_cart_price")
    MobileElement itemPriceField;
    @AndroidFindBy(id = "put_into_cart_button")
    MobileElement addItemButton;
    @AndroidFindBy(id = "add_into_cart_back")
    MobileElement backButton;

    public AddCartModal(AndroidDriver<AndroidElement> driver) {
        super(driver);
    }

    public void clickAddButton(){
        addItemButton.click();
    }

    public void clickBackButton(){
        backButton.click();
    }

    public String getItemName(){
        return itemName.getText();
    }

    public String getItemQuantityAndUnit(){
        return inListQuantity.getText();
    }

    public void typeInCartField(float quantity){
        inCartQuantityField.sendKeys(String.valueOf(quantity));
    }

    public void typePrice(float price){
        itemPriceField.sendKeys(String.valueOf(price));
    }
}
