package com.example.shoppinglist.functionalTest.Tests;

import com.example.shoppinglist.functionalTest.Layout.AddCartModal;
import com.example.shoppinglist.functionalTest.Layout.AddItemActivity;
import com.example.shoppinglist.functionalTest.Layout.DeleteAlertBox;
import com.example.shoppinglist.functionalTest.Layout.ItemMenuModal;
import com.example.shoppinglist.functionalTest.Layout.MainActivity;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.ScreenOrientation;

public class DisplayTest extends TestBody {
    private MainActivity mainActivity;
    private AddItemActivity addItemActivity;
    private ItemMenuModal itemMenuModal;
    private DeleteAlertBox deleteAlertBox;
    private AddCartModal addCartModal;

    @BeforeEach
    public void setupLayout(){
        mainActivity = new MainActivity(driver);
        addItemActivity = new AddItemActivity(driver);
        itemMenuModal = new ItemMenuModal(driver);
        deleteAlertBox = new DeleteAlertBox(driver);
        addCartModal = new AddCartModal(driver);
    }

    @Test
    public void changeLanguage(){
        mainActivity.waitForActivity();
        mainActivity.clickLanguageButton();
        mainActivity.waitLanguageChangeBoxByLanguageMessage();
        mainActivity.clickLanguage("Deutsche");
        Assertions.assertEquals(mainActivity.getAddButtonText(), "HINZUFÜGEN");
        mainActivity.clickLanguageButton();
        mainActivity.waitLanguageChangeBoxByLanguageMessage();
        mainActivity.clickLanguage("English(US)");
        Assertions.assertEquals(mainActivity.getAddButtonText(), "ADD");
        mainActivity.clickLanguageButton();
        mainActivity.waitLanguageChangeBoxByLanguageMessage();
        mainActivity.clickLanguage("Magyar");
        Assertions.assertEquals(mainActivity.getAddButtonText(), "HOZZÁAD");
    }

    @Test
    public void changeLayoutTest(){
        mainActivity.waitForActivity();
        mainActivity.clickAddButton();
        addItemActivity.waitForActivity();
        addItemActivity.typeItemName("Peach");
        addItemActivity.typeItemQuantity(4);
        addItemActivity.selectUnitQuantity();
        addItemActivity.clickAddButton();
        mainActivity.waitForActivity();
        driver.rotate(ScreenOrientation.LANDSCAPE);
        mainActivity.waitForActivity();
        Assertions.assertTrue(mainActivity.checkItemDisplayByName("Peach"));
        Assertions.assertTrue(mainActivity.checkItemInListQuantity("Peach", 4, "DB"));
        Assertions.assertTrue(mainActivity.checkItemInCartQuantity("Peach", 0, "DB"));
        Assertions.assertTrue(mainActivity.checkPrice("Peach", 0));
        mainActivity.clickElement("Peach");
        itemMenuModal.clickDeleteButton();
        deleteAlertBox.acceptDelete();
        Assertions.assertTrue(mainActivity.checkElementDisappearByName("Peach"));
        driver.rotate(ScreenOrientation.PORTRAIT);
    }

    @Test
    public void fullPriceTest(){
        mainActivity.waitForActivity();
        mainActivity.clickAddButton();
        addItemActivity.waitForActivity();
        addItemActivity.typeItemName("Peach");
        addItemActivity.typeItemQuantity(4);
        addItemActivity.selectUnitQuantity();
        addItemActivity.clickAddButton();
        mainActivity.waitForActivity();
        Assertions.assertTrue(mainActivity.checkItemDisplayByName("Peach"));
        Assertions.assertEquals(mainActivity.getFullPrice(), 0);
        mainActivity.clickElement("Peach");
        itemMenuModal.clickIntoCartButton();
        addCartModal.typeInCartField(4);
        addCartModal.typePrice(200);
        addCartModal.clickAddButton();
        itemMenuModal.clickBackButton();
        Assertions.assertEquals(mainActivity.getFullPrice(), 800);
        mainActivity.clickElement("Peach");
        itemMenuModal.clickDeleteButton();
        deleteAlertBox.acceptDelete();
        Assertions.assertTrue(mainActivity.checkElementDisappearByName("Peach"));
    }

    @Test
    public void payTestWithFullCart(){
        mainActivity.waitForActivity();
        mainActivity.clickAddButton();
        addItemActivity.waitForActivity();
        addItemActivity.typeItemName("Peach");
        addItemActivity.typeItemQuantity(4);
        addItemActivity.selectUnitQuantity();
        addItemActivity.clickAddButton();
        mainActivity.waitForActivity();
        Assertions.assertTrue(mainActivity.checkItemDisplayByName("Peach"));
        mainActivity.clickElement("Peach");
        itemMenuModal.clickIntoCartButton();
        addCartModal.typeInCartField(4);
        addCartModal.typePrice(200);
        addCartModal.clickAddButton();
        itemMenuModal.clickBackButton();
        Assertions.assertEquals(mainActivity.getFullPrice(), 800);
        mainActivity.clickPayingButton();
        Assertions.assertTrue(mainActivity.checkElementDisappearByName("Peach"));
        Assertions.assertEquals(mainActivity.getFullPrice(), 0);

    }

    @Test
    public void payTestWithPartQuantity(){
        mainActivity.waitForActivity();
        mainActivity.clickAddButton();
        addItemActivity.waitForActivity();
        addItemActivity.typeItemName("Peach");
        addItemActivity.typeItemQuantity(4);
        addItemActivity.selectUnitQuantity();
        addItemActivity.clickAddButton();
        mainActivity.waitForActivity();
        Assertions.assertTrue(mainActivity.checkItemDisplayByName("Peach"));
        mainActivity.clickElement("Peach");
        itemMenuModal.clickIntoCartButton();
        addCartModal.typeInCartField(2);
        addCartModal.typePrice(200);
        addCartModal.clickAddButton();
        itemMenuModal.clickBackButton();
        mainActivity.clickPayingButton();
        Assertions.assertTrue(mainActivity.checkItemDisplayByName("Peach"));
        Assertions.assertTrue(mainActivity.checkItemInListQuantity("Peach", 2, "DB"));
        Assertions.assertTrue(mainActivity.checkItemInCartQuantity("Peach", 0, "DB"));
        Assertions.assertTrue(mainActivity.checkPrice("Peach", 0));
        mainActivity.clickElement("Peach");
        itemMenuModal.clickDeleteButton();
        deleteAlertBox.acceptDelete();
    }

    @Test
    public void payTestWithMissingQuantity(){
        mainActivity.waitForActivity();
        mainActivity.clickAddButton();
        addItemActivity.waitForActivity();
        addItemActivity.typeItemName("Peach");
        addItemActivity.typeItemQuantity(4);
        addItemActivity.selectUnitQuantity();
        addItemActivity.clickAddButton();
        mainActivity.waitForActivity();
        Assertions.assertTrue(mainActivity.checkItemDisplayByName("Peach"));
        mainActivity.clickPayingButton();
        Assertions.assertTrue(mainActivity.checkItemDisplayByName("Peach"));
        Assertions.assertTrue(mainActivity.checkItemInListQuantity("Peach", 4, "DB"));
        Assertions.assertTrue(mainActivity.checkItemInCartQuantity("Peach", 0, "DB"));
        Assertions.assertTrue(mainActivity.checkPrice("Peach", 0));
        mainActivity.clickElement("Peach");
        itemMenuModal.clickDeleteButton();
        deleteAlertBox.acceptDelete();
    }
}
