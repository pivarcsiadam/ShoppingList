package com.example.shoppinglist.functionalTest.Layout;


import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class DeleteAlertBox extends Layout {
    @AndroidFindBy(id = "reject_delete")
    MobileElement rejectDeleteButton;
    @AndroidFindBy(id = "accept_delete")
    MobileElement acceptDeleteButton;


    public DeleteAlertBox(AndroidDriver<AndroidElement> driver) {
        super(driver);
    }

    public void rejectDelete(){
        rejectDeleteButton.click();
    }

    public void acceptDelete(){
        acceptDeleteButton.click();
    }
}
