package com.example.shoppinglist.functionalTest.Tests;

import com.example.shoppinglist.functionalTest.Layout.AddItemActivity;
import com.example.shoppinglist.functionalTest.Layout.DeleteAlertBox;
import com.example.shoppinglist.functionalTest.Layout.EditItemModal;
import com.example.shoppinglist.functionalTest.Layout.ItemMenuModal;
import com.example.shoppinglist.functionalTest.Layout.MainActivity;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class EditItemTest extends TestBody {
    private MainActivity mainActivity;
    private EditItemModal editItemModal;
    private ItemMenuModal itemMenuModal;
    private DeleteAlertBox deleteAlertBox;
    private AddItemActivity addItemActivity;

    @BeforeEach
    public void setupPreconditions(){
        mainActivity = new MainActivity(driver);
        editItemModal = new EditItemModal(driver);
        itemMenuModal = new ItemMenuModal(driver);
        deleteAlertBox = new DeleteAlertBox(driver);
        addItemActivity = new AddItemActivity(driver);

        mainActivity.waitForActivity();
        mainActivity.clickAddButton();
        addItemActivity.waitForActivity();
        addItemActivity.typeItemName("Peach");
        addItemActivity.typeItemQuantity(4);
        addItemActivity.selectUnitQuantity();
        addItemActivity.clickAddButton();
        mainActivity.waitForActivity();
        Assertions.assertTrue(mainActivity.checkItemDisplayByName("Peach"));
    }

    @Test
    public void editItemHappyWay(){
        mainActivity.clickElement("Peach");
        itemMenuModal.clickEditButton();
        Assertions.assertEquals(editItemModal.getItemName(), "Peach");
        Assertions.assertEquals(editItemModal.getItemQuantity(), 4);
        editItemModal.editItemName("Orange");
        editItemModal.editInListQuantity(2);
        editItemModal.selectWeighUnit();
        editItemModal.clickSaveButton();
        itemMenuModal.clickBackButton();
        Assertions.assertTrue(mainActivity.checkItemDisplayByName("Orange"));
        Assertions.assertTrue(mainActivity.checkItemInListQuantity("Orange", 2, "KG"));
        Assertions.assertTrue(mainActivity.checkItemInCartQuantity("Orange", 0, "KG"));
        Assertions.assertTrue(mainActivity.checkPrice("Orange", 0));
        mainActivity.clickElement("Orange");
        itemMenuModal.clickEditButton();
        editItemModal.editItemName("Peach");
        editItemModal.clickSaveButton();
        itemMenuModal.clickBackButton();
    }

    @Test
    public void editFailByEmptyNameField(){
        mainActivity.clickElement("Peach");
        itemMenuModal.clickEditButton();
        editItemModal.editItemName("");
        editItemModal.clickSaveButton();
        mainActivity.waitForToast();
        Assertions.assertEquals(mainActivity.getToastMessage(), "Ne hagyj üresen mezőt!");
        editItemModal.clickBackButton();
        itemMenuModal.clickBackButton();
        Assertions.assertTrue(mainActivity.checkItemDisplayByName("Peach"));
    }

    @Test
    public void editFailByEmptyQuantityField(){
        mainActivity.clickElement("Peach");
        itemMenuModal.clickEditButton();
        editItemModal.clearQuantityField();
        editItemModal.clickSaveButton();
        mainActivity.waitForToast();
        Assertions.assertEquals(mainActivity.getToastMessage(), "Ne hagyj üresen mezőt!");
        editItemModal.clickBackButton();
        itemMenuModal.clickBackButton();
        Assertions.assertTrue(mainActivity.checkItemDisplayByName("Peach"));
    }

    @Test
    public void editFailByRepeatedItem(){
        mainActivity.clickAddButton();
        addItemActivity.waitForActivity();
        addItemActivity.typeItemName("Orange");
        addItemActivity.typeItemQuantity(4);
        addItemActivity.selectUnitQuantity();
        addItemActivity.clickAddButton();
        mainActivity.waitForActivity();
        Assertions.assertTrue(mainActivity.checkItemDisplayByName("Orange"));

        mainActivity.clickElement("Orange");
        itemMenuModal.clickEditButton();
        editItemModal.editItemName("Peach");
        editItemModal.clickSaveButton();
        mainActivity.waitForToast();
        Assertions.assertEquals(mainActivity.getToastMessage(), "Ezt a terméket már hozzáadtad a listához!");
        editItemModal.clickBackButton();
        itemMenuModal.clickBackButton();
        Assertions.assertTrue(mainActivity.checkItemDisplayByName("Orange"));

        mainActivity.clickElement("Orange");
        itemMenuModal.waitForModal();
        itemMenuModal.clickDeleteButton();
        deleteAlertBox.acceptDelete();
        Assertions.assertTrue(mainActivity.checkElementDisappearByName("Orange"));
    }

    @AfterEach
    public void clearTable(){
        mainActivity.clickElement("Peach");
        itemMenuModal.waitForModal();
        itemMenuModal.clickDeleteButton();
        deleteAlertBox.acceptDelete();
        Assertions.assertTrue(mainActivity.checkElementDisappearByName("Peach"));
    }
}
